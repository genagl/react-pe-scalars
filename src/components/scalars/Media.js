import React from "react" 
import ScalarField from "./ScalarField"
import {MediaChooser} from "react-pe-useful" 
import { compose } from "recompose"
import { withApollo } from "react-apollo"
import { server_url__ } from "settings/config"

//  Scalar  String

class Media extends ScalarField {

  isEnabled() {
    const { field } = this.props
    const { value } = this.state
    return <div className="my-2 w-100">
      <MediaChooser
        prefix={`_${field}${this.props.id}`}
        url={value}
        id=""
        ID=""
        padding={5}
        height={120}
        isUploadHide={this.props.isUploadHide}
        isURLHide={this.props.isURLHide}
        onChange={this.onMediaChange}
        server_url={server_url__()}
      />
    </div>
  }

  isDesabled() {
    const { value } = this.state
    return <div className="px-0 my-1">
      <div
        style={{
          backgroundImage: `url(${value})`,
          backgroundSize: "cover",
          width: 60,
          height: 60,
          opacity: 0.8,
          margin: 6,
        }}
      />
    </div>
  }

  onMediaChange = (value, file, id) => {
    // console.log(value);
    const state = { value }
    // state[this.props.field + "_name"] =

    this.on(value, this.props.field, file.name, id)
    this.setState(state)
  }

  on = (value, name, fileName, id=-1) => {
    const anoverField = {}
    anoverField[`${this.props.field}_id`] = id
    anoverField[`${this.props.field}_name`] = fileName

    // console.log(  value, this.props.field, this.props.title, anoverField );
    this.props.on(value, this.props.field, this.props.title, anoverField)
  }
}

export default  compose(
	withApollo, 
)(Media)
