import React from "react"
import { Tag } from "@blueprintjs/core"
import { __ } from "react-pe-utilities" 
import ScalarField from "./ScalarField"
import { getTypeSelector } from "react-pe-layouts"

export default class DataTypeSelector extends ScalarField {
  constructor(props) {
    super(props)
    this.state = {
      value: this.props.value,
    }
  }

  isDesabled() {
    return (
      <div className={`datetimer ${this.props.className}`}>
        <div className="px-0 my-2">
          {
				this.props.value
				  ? (
            <Tag minimal>
              { `${this.props.value} `}
            </Tag>
				  )
				  :				
          null
			}
        </div>
      </div>
    )
  }

  isEnabled() {
    return (
      <div className={`datetimer w-100 ${this.props.className}`}>
        { getTypeSelector({ onChange: this.onChange, selected: this.state.value }) }
      </div>
    )
  }

	onChange = (evt) => {
	  this.setState({ value: evt.currentTarget.value })
	  this.on(evt.currentTarget.value)
	}

	on = (value) => {
	  this.props.on(value, this.props.field, this.props.title)
	}
}
