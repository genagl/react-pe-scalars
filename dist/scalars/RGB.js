function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "react";
import { __ } from "react-pe-utilities";
import { ColorPicker } from "react-pe-useful"; // TODO extends ScalarField

export default class RGB extends Component {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "state", {
      value: this.props.value
    });

    _defineProperty(this, "color", color => {
      this.setState({
        value: color.hex
      });
      this.on(color.hex);
    });

    _defineProperty(this, "on", value => {
      this.props.on(value, this.props.field, this.props.title);
    });
  }

  render() {
    const {
      field,
      title
    } = this.props;
    const col1 = this.props.vertical ? "col-12 layout-label-vert" : "col-md-3  layout-label";
    const col2 = this.props.vertical ? "col-12 layout-data-vert" : "col-md-7 layout-data";
    return /*#__PURE__*/React.createElement("div", {
      className: "row dat",
      key: field
    }, /*#__PURE__*/React.createElement("div", {
      className: col1
    }, __(title)), /*#__PURE__*/React.createElement("div", {
      className: col2
    }, this.props.editable ? /*#__PURE__*/React.createElement(ColorPicker, {
      color: this.state.value,
      onChoose: this.color
    }) : /*#__PURE__*/React.createElement("div", {
      style: {
        width: 36,
        height: 14,
        borderRadius: 2,
        backgroundColor: this.state.value
      },
      className: " my-2"
    })));
  }

}