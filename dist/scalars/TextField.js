function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React from "react";
import { Tag, Intent, TextArea } from "@blueprintjs/core";
import { __ } from "react-pe-utilities";
import ScalarField from "./ScalarField";
export default class TextField extends ScalarField {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "onChange", evt => {
      this.setState({
        value: evt.currentTarget.value
      });
      this.on(evt.currentTarget.value);
    });

    _defineProperty(this, "on", value => {
      this.props.on(value, this.props.field, this.props.title);
    });
  }

  isEnabled() {
    const {
      value
    } = this.state;
    return /*#__PURE__*/React.createElement(TextArea, {
      value: value || "",
      onChange: this.onChange,
      growVertically: true,
      fill: true,
      large: true,
      style: {
        minHeight: 350
      },
      intent: Intent.PRIMARY,
      className: " p-4 "
    });
  }

  isDesabled() {
    return /*#__PURE__*/React.createElement("div", {
      className: "px-0 my-2"
    }, this.props.value ? /*#__PURE__*/React.createElement(Tag, {
      minimal: true
    }, `${this.props.value} `) : null);
  }

}