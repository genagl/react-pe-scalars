import React from "react";
import { Button, Tag } from "@blueprintjs/core";
import { __ } from "react-pe-utilities";
import ScalarField from "./ScalarField"; // TODO extends ScalarField

export default class URL extends ScalarField {
  isEnabled() {
    const {
      value
    } = this.state;
    return /*#__PURE__*/React.createElement("div", {
      className: "w-100 d-flex align-items-center"
    }, /*#__PURE__*/React.createElement("i", {
      className: "fas fa-desktop"
    }), /*#__PURE__*/React.createElement("input", {
      type: "text",
      className: "form-control input dark",
      value: value || "",
      onChange: this.onChange
    }), /*#__PURE__*/React.createElement(Button, {
      className: "right",
      icon: "cross",
      minimal: true,
      onClick: this.onClear
    }));
  }

  isDesabled() {
    return /*#__PURE__*/React.createElement("div", {
      className: "px-0 my-2"
    }, this.props.value ? /*#__PURE__*/React.createElement(Tag, {
      minimal: true
    }, `${this.props.value} `) : null);
  }

}