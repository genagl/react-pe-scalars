function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "react";
import { Tag, ButtonGroup, Button, Intent } from "@blueprintjs/core";
import { __ } from "react-pe-utilities";
import ScalarField from "./ScalarField";
import { MediaChooser } from "react-pe-useful"; //  Scalar  String

export default class Upload extends ScalarField {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "onMediaChange", (value, file) => {
      // console.log(file);
      const state = {
        value: file
      };
      this.on(file, this.props.field, file.name);
      this.setState(state);
    });

    _defineProperty(this, "onChange", evt => {
      console.log(evt.currentTarget);
      console.log(evt.target.files);
      const file = evt.target.files[0];
      this.setState({
        value: file
      });
      this.on(file);
    });

    _defineProperty(this, "on", (value, name, fileName) => {
      this.props.on(value, this.props.field, this.props.title);
    });
  }

  render() {
    const {
      field,
      title
    } = this.props;
    const {
      value
    } = this.state;
    const col1 = this.props.vertical ? "col-12 layout-label-vert" : "col-md-3  layout-label";
    const col2 = this.props.vertical ? "col-12 layout-data-vert" : "col-md-7 layout-data";
    return /*#__PURE__*/React.createElement("div", {
      className: "row dat",
      key: field
    }, /*#__PURE__*/React.createElement("div", {
      className: col1
    }, __(title)), /*#__PURE__*/React.createElement("div", {
      className: col2
    }, this.props.editable ? /*#__PURE__*/React.createElement("div", {
      className: "my-2"
    }, /*#__PURE__*/React.createElement(MediaChooser, {
      prefix: `_${field}${this.props.id}`,
      url: value,
      id: "",
      ID: "",
      isDescr: true,
      padding: 5,
      height: 120,
      onChange: this.onMediaChange
    }), /*#__PURE__*/React.createElement("input", {
      type: "hidden",
      onChange: this.onChange
    })) : /*#__PURE__*/React.createElement("div", {
      className: "px-0 my-1"
    }, /*#__PURE__*/React.createElement("div", {
      style: {
        backgroundImage: `url(${value})`,
        backgroundSize: "cover",
        width: 60,
        height: 60,
        opacity: 0.8,
        margin: 6
      }
    }))));
  }

}