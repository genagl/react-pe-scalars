function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "react";
import { __ } from "react-pe-utilities"; // TODO extends ScalarField

export default class Radio extends Component {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "state", {
      value: this.props.value,
      data: [{
        _id: 11,
        title: "AAAA"
      }, {
        _id: 12,
        title: "BBBB"
      }, {
        _id: 13,
        title: "CCCC"
      }, {
        _id: 14,
        title: "DDDD"
      }, {
        _id: 15,
        title: "EEEE"
      }]
    });

    _defineProperty(this, "onChange", evt => {
      const {
        value
      } = evt.currentTarget;
      this.setState({
        value
      });
      this.on(value);
    });

    _defineProperty(this, "on", value => {
      // console.log(value, this.props.field, this.props.title);
      this.props.on(value, this.props.field, this.props.title);
    });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.value) this.setState({
      value: nextProps.value
    });
  }

  getComponent() {
    const {
      component,
      values
    } = this.props;
    const {
      value
    } = this.state; // console.log( value );

    let _component;

    if (typeof component === "String") {
      _component = this.state.data;
    } else if (component === "PlaceType") {
      const items = Array.isArray(values) ? values : Array.isArray(component) ? component : null; // console.log(items);

      _component = items ? items.map((e, i) => {
        const elem = typeof e._id != "undefined" ? e : {
          _id: e._id,
          title: e.title,
          description: ""
        }; //const id = `__${this.props._id}_${elem._id}`

        return /*#__PURE__*/React.createElement("div", {
          className: "pb-0 mb-1",
          key: i
        }, /*#__PURE__*/React.createElement("label", {
          className: "_check_blue_"
        }, /*#__PURE__*/React.createElement("input", {
          value: elem._id,
          type: "radio",
          checked: elem._id === value,
          onChange: this.onChange
        }), __(elem.title)), /*#__PURE__*/React.createElement("div", null, __(elem.description)));
      }) : null;
    } else {
      const items = Array.isArray(values) ? values : Array.isArray(component) ? component : [values]; // console.log(items, values, value);

      _component = items ? items.map((e, i) => {
        const elem = typeof e._id != "undefined" ? e : {
          _id: e,
          title: __(e)
        };
        const id = `__${this.props._id}_${elem._id}`; // console.log(elem._id, value, elem._id == value);

        return /*#__PURE__*/React.createElement("div", {
          className: "pb-0 mb-1",
          key: i
        }, /*#__PURE__*/React.createElement("label", {
          className: "_check_blue_",
          htmlFor: id
        }, /*#__PURE__*/React.createElement("input", {
          value: elem._id,
          type: "radio",
          checked: elem._id === this.props.value,
          onChange: this.onChange,
          onClick: this.onChange,
          id: id
        }), __(elem.title)));
      }) : null;
    }

    return _component;
  }

  render() {
    const {
      field,
      title
    } = this.props;
    const selecting = this.getComponent();
    const col1 = this.props.vertical ? "col-12 layout-label-vert" : "col-md-3  layout-label";
    const col2 = this.props.vertical ? "col-12 layout-data-vert" : "col-md-7 layout-data";
    return /*#__PURE__*/React.createElement("div", {
      className: "row dat",
      key: field
    }, /*#__PURE__*/React.createElement("div", {
      className: col1
    }, __(title)), /*#__PURE__*/React.createElement("div", {
      className: col2
    }, this.props.editable ? /*#__PURE__*/React.createElement("div", {
      className: "my-2"
    }, selecting) : /*#__PURE__*/React.createElement("div", {
      className: "px-0 my-1"
    }, this.props.value)));
  }

}